
var initialState=[
    {
        id:1,
        name:"Iphone 7 Plus",
        img:'https://cdn.tgdd.vn/Products/Images/42/87839/iphone-7-plus-128gb-hh-600x600.jpg',
        description:"Sản phẩm do Apple sản xuất",
        price:500,
        inventory:10,
        rate:5
    },
    {
        id:2,
        name:"Samsung galaxy S7",
        img:'https://didongviet.vn/pub/media/catalog/product//s/a/samsung-galaxy-s7-my-den-didongviet_3_1.jpg',
        description:"Sản phẩm do Samsung sản xuất",
        price:400,
        inventory:15,
        rate:5
    },
    {
        id:3,
        name:"Oppo F1s",
        img:'https://cdn.fptshop.com.vn/Uploads/Originals/2017/2/9/636222506137676408_f1s-2.jpg',
        description:"Sản phẩm do China sản xuất",
        price:450,
        inventory:5,
        rate:3
    },
]

const products=(state=initialState,action)=>{
    switch (action.type) {
        default:
            return [...state];
    }
}

export default products;  